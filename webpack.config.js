module.exports = {
    entry: "./main.js",
    module:{
        loaders:[{
            loader:'babel',

            test: /\.js$/,
            exclude: /node_modules/,
            query: {
                presets: ['es2015','react']
            }
        }]
    },
    output: {
        path: __dirname + "/build",
        filename: "bundle.js"
    },
    devtool: "source-map"
};