from tornado import websocket, ioloop, web, httpserver

import json





def generate_websocket_handler(connections_pool):

    class EchoHandler(websocket.WebSocketHandler):
        def check_origin(self, origin):
            return True

        def __init__(self,*args,**kwargs):
            super(EchoHandler,self).__init__(*args,**kwargs)

            self.connections_pool = connections_pool
        def open(self):

            if self not in self.connections_pool:
                self.connections_pool[self]={'user_id':None}

        def on_connection_close(self):
            if self in self.connections_pool:
                self.connections_pool.pop(self)

        def on_message(self, message):
            data = json.loads(message)
            print (data)
            print (connections_pool, self)
            for connection in self.connections_pool:

                if not (connection is self):
                    connection.write_message(message)

    return EchoHandler




class IndexHandler(web.RequestHandler):
    def get(self):
        with open('index.html') as hf:
            html = hf.read()
        return self.write(html)

class JsHandler(web.RequestHandler):
    def get(self):
        with open('build/bundle.js') as hf:
            html = hf.read()
        return self.write(html)


def main():

    connections_pool={}


    app = web.Application([

        (r'/ws',generate_websocket_handler(connections_pool)),
        (r'/', IndexHandler),
        (r'/build/bundle.js', JsHandler)


    ], debug=True, static_path='build/')

    app.listen(9999)
    ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()