require('babel-polyfill')

const { createStore } = require('redux');


const {Component} = require("react");
const {Button,ControlLabel,Form,FormControl,Col,Grid,Row} = require('react-bootstrap');

var React = require('react');
var ReactDOM = require('react-dom');



const INITIAL_STATE={
    messages:[{
        user_name:'A1',
        text:'First mess'
    },
        {
            user_name:'A2',
            text:'Second mess'
        }
    ],
    user_name:'Anonymous',
    authorized:false,
    temp:''

}



const chat_reducer = (state=INITIAL_STATE,action)=>{

    switch (action.type) {
        case 'SET_NAME':
            return {
                messages:state.messages,
                user_name:action.name,
                authorized:state.authorized,
                temp:state.temp
            };

        case 'AUTHORIZE':
            return {
                messages:state.messages,
                user_name:state.user_name,
                authorized:true,
                temp:state.temp
            };

        case 'ADD_MESSAGE':{
            if ($.trim(state.temp)==''){return state}
            return {
                messages:[...state.messages,{
                    text:state.temp,
                    user_name:state.user_name}],
                user_name:state.user_name,
                authorized:true,
                temp:''
            }
        };

        case 'CHANGE_NEW_MESSAGE':
            return Object.assign({},state,{
                temp:action.value

            });

        case 'SET_MESSAGES':
            return Object.assign({},state,{
                messages:action.messages

            });

        case 'DELETE_MESSAGE':
            return state;
        default:
            return state
    }

}

class ChatWindow extends Component {

    render () {

        const state=this.props.store.getState();

        return (<Grid>
            <Row>
                {(()=>{if (state.authorized)
                {return (<Col xs={6}>You are authorized as {state.user_name}</Col>)}
                else
                {

                    return (<Form id="b" inline><ControlLabel>Enter your name: </ControlLabel>

                        <FormControl type="text" value={state.user_name} onChange={(e)=>
                        this.props.store.dispatch({
                            type:'SET_NAME',
                            name:e.target.value

                        })

                    }/><Button onClick={()=>

                        this.props.store.dispatch({
                            type:'AUTHORIZE',
                        })

                    }>Set Name</Button></Form>)


                }


                })()}
            </Row>
            {(()=>{



                return state.messages.map((message)=>(<Row>
                <Col xs={2}>{message.user_name}</Col><Col xs={4}>{message.text}</Col>
                </Row>))})()}
            <Row>
                <Form id="e" inline><ControlLabel>Enter your new message</ControlLabel>
                <FormControl type="text" value={state.temp}
                             onChange={(e)=>
                                 this.props.store.dispatch({
                                     type:'CHANGE_NEW_MESSAGE',
                                     value:e.target.value

                                 })

                             }/>
                </Form>
                <Form id="c"><Button
                    onClick={()=>

                        this.props.store.dispatch({
                            type:'ADD_MESSAGE'
                        })

                    }>
                    Send Message
                </Button>
                </Form>

            </Row>

        </Grid>)


    }


}

const store = createStore(chat_reducer);

const render = () => {


    ReactDOM.render(


        <ChatWindow store={store} state={store.getState()} dispatch={store.dispatch}/>,
        document.getElementById('root')

    );

}


const connection = new WebSocket('ws://'+window.location.host+window.location.pathname+'ws');

connection.onmessage = (e)=>{

    var data = JSON.parse(e.data);

    var in_mess_state={mes:data.messages};
    var mess_state={mes:store.getState().messages}

    if (JSON.stringify(in_mess_state) !== JSON.stringify(mess_state)){

    store.dispatch({
        type:'SET_MESSAGES',
        messages:data.messages

    })}

};

const send_state_to_server =()=>{

    connection.send(JSON.stringify(store.getState()))


};

store.subscribe(send_state_to_server);


store.subscribe(render);
window.onload = render ;